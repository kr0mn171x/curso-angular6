import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje } from './.././../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  /* destinos: string[];*/
  //destinos: DestinoViaje[];
  //constructor() { 
 	/* this.destinos = ['Barranquilla','Lima','Buenos Aires','Barcelona']; */
 	//this.destinos = [];
  //}

  constructor(public destinosApiClient:DestinosApiClient, public store: Store<AppState>) { 
    /* this.destinos = ['Barranquilla','Lima','Buenos Aires','Barcelona']; */
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push('Se ha elegido a ' + d.nombre);
        }
      });
      this.all = store.select(state => state.destinos.items).subscribe(items => this.all = items);
      /*
    this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
      if (d != null) {
        this.updates.push('Se ha elegido a ' + d.nombre);
      }
    });*/ 
   }

   /*
  ngOnInit(): void {
  }
  */
  ngOnInit() {
  }
  
  //guardar(nombre:string, url:string):boolean{
  	/* console.log(nombre); */
  	/* console.log(url); */
  	//this.destinos.push(new DestinoViaje(nombre, url));
  	/* console.log(new DestinoViaje(nombre, url)); */
  	/* console.log(this.destinos); */
  	//return false;
  //}

  //guardar(nombre:string, url:string):boolean{
  	/* console.log(nombre); */
  	/* console.log(url); */
    //let d = new DestinoViaje(nombre, url);
    //this.destinosApiClient.add(d);
    //this.onItemAdded.emit(d);
  	/* console.log(new DestinoViaje(nombre, url)); */
  	/* console.log(this.destinos); */
  	//return false;
  //}

  agregado(d: DestinoViaje){
  	/* console.log(nombre); */
  	/* console.log(url); */
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  	/* console.log(new DestinoViaje(nombre, url)); */
    /* console.log(this.destinos); */
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  /*
  elegido(d: DestinoViaje) {
    this.destinos.forEach(function (x) {x.setSelected(false); });
     d.setSelected(true); 
    }
    */
/*
   elegido(e: DestinoViaje) {
    this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
     e.setSelected(true); 
    }
 */   

  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);
    //this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll() {

  }
}
