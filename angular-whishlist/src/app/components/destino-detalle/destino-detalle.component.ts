import { Component, Inject, OnInit, InjectionToken, Injectable } from '@angular/core';
import { DestinosApiClient } from './.././../models/destinos-api-client.model';
import { DestinoViaje } from './.././../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';

class DestinosApiClientViejo {
  getById(id: string): DestinoViaje {
    console.log('llamado por la clase vieja');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
};
const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(@Inject(APP_CONFIG) public config: AppConfig, store: Store<AppState>) {
    super(store);
  }
  getById(id): DestinoViaje {
    console.log('llamado por la clase decorada!');
    console.log('config: ' + this.config.apiEndpoint);
    return super.getById(id);
  }
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  //providers: [
    //DestinosApiClient, 
    //{ provide: DestinosApiClient, useExisting: DestinosApiClient } 
    //{ provide: DestinosApiClientViejo, useExisting: DestinosApiClient } 
  //]
  providers: [ 
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    { provide: DestinosApiClientViejo, useExisting: DestinosApiClient } 
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  //constructor(public route: ActivatedRoute, public destinosApiClient: DestinosApiClient) { }
  constructor(public route: ActivatedRoute, public destinosApiClient: DestinosApiClientViejo) { }

  ngOnInit() {
    //let id = this.route.snapshot.paramMap.get('id');
    const id = this.route.snapshot.paramMap.get('id');
    //this.destino = null;
    this.destino = this.destinosApiClient.getById(id);
  }

}
