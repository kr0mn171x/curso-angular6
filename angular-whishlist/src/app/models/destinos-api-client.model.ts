import { DestinoViaje } from './destino-viaje.model';
//import { Subject, BehaviorSubject } from 'rxjs';
//import { NuevoDestinoAction } from './destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable } from '@angular/core';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];
  //destinos:DestinoViaje[];
  //current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
  //constructor() {
    constructor(public store: Store<AppState>) {
      //this.destinos = [];
      this.store
        .select(state => state.destinos)
        .subscribe((data) => {
          console.log('destinos sub store');
          console.log(data);
          this.destinos = data.items;
        });
      this.store
        .subscribe((data) => {
          console.log('all store');
          console.log(data);
        });
  }
  /*
  add(d: DestinoViaje){
    this.destinos.push(d);
  }
  */
  add(d: DestinoViaje){
    this.store.dispatch(new NuevoDestinoAction(d));
  }
  /*
  getAll():DestinoViaje[]{
    return this.destinos;
  }
  */
  
  getById(id:string):DestinoViaje{
    return this.destinos.filter(function(d){return d.isSelected.toString() == id;})[0];
  }
  
  elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
  /*
  elegir(d: DestinoViaje) {
    this.destinos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    this.current.next(d);
  }
  */
 /*
  subscribeOnChange(fn) {
    this.current.subscribe(fn);
  }
  */
}