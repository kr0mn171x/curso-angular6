import { v4 as uuid } from 'uuid';

/*
export class DestinoViaje {
	nombre: string;
	imagenUrl: string;

	constructor(n:string, u:string){
		this.nombre = n;
		this.imagenUrl = u;
	}
}*/

/*
export class DestinoViaje {
	private selected: boolean;
	constructor(public nombre: string, public u: string) { }
	isSelected(): boolean {
		return this.selected;
	}
	setSelected() {
		this.selected = true;
	}
}
*/

/*
export class DestinoViaje {
	private selected: boolean;
	constructor(public nombre: string, public u: string) {}
	isSelected(): boolean {
		return this.selected;
	}
	setSelected(s: boolean) {
		this.selected = s;
	}
}*/

export class DestinoViaje {
	private selected: boolean;
	public servicios: string[];
	id = uuid();
	//constructor(public nombre: string, public u: string) {
	constructor(public nombre: string, public imagenUrl: string, public votes: number = 0) {
		this.servicios = ['pileta', 'desayuno'];
	}
	setSelected(s: boolean) {
		this.selected = s;
	}
	/*
	isSelected(): boolean {
		return this.selected;
	}
	*/
	isSelected() {
		return this.selected;
	}
	voteUp(): any {
		//throw new Error("Method not implmeneted.");
		this.votes++;
	}
	voteDown(): any {
		this.votes--;
	}
}